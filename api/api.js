const BASE_URL = 'http://192.168.3.74:8081/'
export const myRequest = (options)=>{
	return new Promise((resolve,reject)=>{
		let data = options.data
		data.token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMjAiLCJpYXQiOjE2MDU5NDIxMjcsInN1YiI6IjE4NzI5MjUzMzc1IiwidXNlcklkIjoiMTIwIiwidXNlck5hbWUiOiIxODcyOTI1MzM3NSIsImV4cCI6MTYwNzQxMzM1Nn0.3AYSobvEpCqCiEr6AlsdAA2-Py5Eh_3ohUPAWvaugWE"
		uni.request({
			url:BASE_URL+options.url,
			method:options.method || 'GET',
			data:data,
			success: (res) => {
				// console.log(res)
				if(res.data.code == 0){
					// console.log(res,".........................................................")
					resolve(res)	//将返回成功的消息返回
				}else{
					reject("错误")	
				}
				
			},
			fail: (err) => {
				uni.showToast({
					title:'获取数据失败'
				})
				reject(err)		//将返回失败的消息返回
			},
			
		})
	})
}

// myRequest({
// 	url:'/api/getlunbo',
// 	methods:'POST',
// 	data:{
		
// 	}
// })
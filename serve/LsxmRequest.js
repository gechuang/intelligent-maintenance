const config = Symbol('config')
const isCompleteURL = Symbol('isCompleteURL')
const requestBefore = Symbol('requestBefore')
const requestAfter = Symbol('requestAfter')

class LsxmRequest {
	constructor() {
	    this.loginFlag = null
	    this.loginNum = 0
		this.compoent = 0;
		
	}
    //默认配置
    [config] = {
        baseURL: '',
		data:{},
        header: {
            // 'content-type': 'application/json'
			
        },
        method: 'GET',
        dataType: 'json',
        responseType: 'text'
    }
    //拦截器
    interceptors = {
        request: (func) => {
            if (func) {
                LsxmRequest[requestBefore] = func;
            } else {
                LsxmRequest[requestBefore] = (request) => request
            }
        },
        response: (func) => {
            if (func) {
                LsxmRequest[requestAfter] = func
            } else {
                LsxmRequest[requestAfter] = (response) => response
            }
        }
    }
	setloginNum(num){
			return this.loginNum  == this.compoent
	}
    static [requestBefore] (config) {
        return config
    }

    static [requestAfter] (response) {
        return response
    }

    static [isCompleteURL] (url) {
        return /(http|https):\/\/([\w.]+\/?)\S*/.test(url)
    }
    
    request (options = {}) {
		this.loginNum = this.loginNum + 1
		if(this.loginFlag == null){
			uni.showLoading({
				title: "加载中",
				mask: true 
			})
			this.loginFlag = true
		}
        options.baseURL = options.baseURL || this[config].baseURL
        options.dataType = options.dataType || this[config].dataType
        options.url = LsxmRequest[isCompleteURL](options.url) ? options.url : (options.baseURL + options.url)
        options.data = options.data
        options.header = {...options.header, ...this[config].header}
        options.method = options.method || this[config].method
        options = {...options, ...LsxmRequest[requestBefore](options)}
        return new Promise((resolve, reject) => {
            options.success = function (res) {
				uni.hideLoading()
                resolve(LsxmRequest[requestAfter](res))
				
            }
            options.fail= function (err) {
				uni.hideLoading()
                reject(LsxmRequest[requestAfter](err))
            }
			options.complete = function(){
				this.compoent = this.compoent + 1;
				setTimeout(function(){
					if(this.setloginNum()){
						uni.hideLoading()
						this.loginFlag = null;
					}
				}.bind(this))
			}.bind(this)
            uni.request(options)
        })
    }

    get (url, data, options = {}) {
        options.url = url
        options.data = data
        options.method = 'GET'
        return this.request(options)
    }

    post (url, data, options = {}) {
        options.url = url
        options.data = data
        options.method = 'POST';
		options.header = {
			'content-type': 'application/x-www-form-urlencoded'
		}
        return this.request(options)
    }
	
	setConfig (func) {
		this[config] = func(this[config])
	}
	getConfig() {
	    return this[config];
	}
	
}



export default LsxmRequest


import lsxmRequest from '../config.js'
export default{
	/*
		上报困人
	*/
        reportElevatorFault(data){
            return lsxmRequest.post('elevatorFault/reportElevatorFault', data)
        },
		
		/*
			配件申请
		*/
		queryApplicationListCount(data){
			return lsxmRequest.get('partApplication/queryApplicationListCount', data)
		},
		
		/*
			配件申请列表
		*/
		selectPartApplicationList(data){
			return lsxmRequest.get('partApplication/selectPartApplicationList', data)
		},
		
		/*
			配件申请列表
		*/
		selectPartApplicationInfo(data){
			return lsxmRequest.get('partApplication/selectPartApplicationInfo', data)
		},
		
		/*
			通过 退回
		*/
		approvalPartApplication(data){
			return lsxmRequest.post('partApplication/approvalPartApplication', data)
		},
		
		/*
			选择库件名称
		*/
		selectInventoryInfoList(data){
			return lsxmRequest.get('partApplication/selectInventoryInfoList', data)
		},
		
		/*
			选择规格
		*/
		selectInventorySizeList(data){
			return lsxmRequest.get('partApplication/selectInventorySizeList', data)
		},
		
		/*
			选择规格值
		*/
		selectInventorySizePriceList(data){
			return lsxmRequest.get('partApplication/selectInventorySizePriceList', data)
		},
		
		/*
			保存
		*/
		savePartApplication(data){
			return lsxmRequest.post('partApplication/savePartApplication', data)
		},
		
		
		/*
			获取品牌列表 business_elevator_brand
			获取型号列表 business_elevator_model
		*/
		searchDictionList(data){
			return lsxmRequest.get('sysDictionary/searchDictionList', data)
		},
		/**
		 * @param {Object} data 选择电梯
		 */
		selectElevatorBrandList(data){
			return lsxmRequest.get('faultManage/selectElevatorBrandList', data)
		},
		/**
		 * @param {Object} data 选择型号
		 */
		selectElevatorModelList(data){
			return lsxmRequest.get('faultManage/selectElevatorModelList', data)
		},
		
		/**
		 * @param {Object} data 搜索列表
		 */
		selectFaultCode(data){
			return lsxmRequest.get('faultManage/selectFaultCode', data)
		},
		  
}
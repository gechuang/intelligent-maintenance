
import lsxmRequest from '../config.js'
export default{
	/**
	 * 维修
	 * reviewType	是	int	(1.待维修，2.待确认 ，3. 已完成)
			token	是	string	token 校验
			pageNum	是	Integer	页码
			pageSize	是	Integer	当前页展示数量
	 */
        selectListByStatus(data){
            return lsxmRequest.get('elevatorFault/selectListByStatus', data)
        },
	
}	
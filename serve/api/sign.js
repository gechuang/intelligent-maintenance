import lsxmRequest from '../config.js'
export default{
	/*
		1. 查询个人签单记录接口
		
		url : api/userSign/userMineSignList   
		type:  Get
		参数：
		token  
		pageNum ：页数
		pageSize ：条数
		
		返回参数：
		longitude ：经度
		latitude ：纬度
		addressDetails ：详细地址
		createTime ： 签到时间
	*/
        userMineSignList(data){
            return lsxmRequest.get('userSign/userMineSignList', data)
        },
		
		
		/*
			2.  签到接口
			
			url : api/userSign/userSignClockSave
			type:  POST
			传的参数：
			token
			longitude ：经度
			latitude ：纬度
			addressDetails ：详细地址
			
			返回 code: 0 成功
		*/
		userSignClockSave(data){
		    return lsxmRequest.post('userSign/userSignClockSave', data)
		},

}
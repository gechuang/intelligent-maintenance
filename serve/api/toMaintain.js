import lsxmRequest from '../config.js'
export default{
	/*
		保养（待保养，待确认，已完成）
	*/
        selectUpkeepAuditList(data){
            return lsxmRequest.get('upkeep/selectUpkeepAuditList', data)
        },
		
	/*
		查询保养清单页面详情接口 upkeep/selectUpkeepProjectDetails
	*/
		
		selectUpkeepProjectDetails(data){
		    return lsxmRequest.post('upkeep/selectUpkeepProjectDetails', data)
		},
		
	/**
	 * 保养项目列表选中改变状态接口 开关
	 * /api/upkeep/updateUpkeepProjectResult 
	 */
	updateUpkeepProjectResult(data){
	    return lsxmRequest.post('upkeep/updateUpkeepProjectResult', data)
	},
	
	/*
		保养审核提交接口（含提交/审核）
		/api/upkeep/submitUpkeepAuditInfo   POST
	*/
	submitUpkeepAuditInfo(data){
	    return lsxmRequest.post('upkeep/submitUpkeepAuditInfo', data)
	},
	
	startUpkeep(data){
	    return lsxmRequest.post('upkeep/startUpkeep', data)
	},
	//查询用户信息接口
	selectSysUserById(data){
	    return lsxmRequest.get('sysUser/selectSysUserById', data)
	},
	//用户信息修改接口
	updateUserInfo(data){
	    return lsxmRequest.post('sysUser/updateUserInfo', data)
	},
}
import lsxmRequest from '../config.js'
export default{
	/**
	 * token	是	string	用户凭证
		communityManage	否	CommunityManage	社区实体对象(需要搜索时传入相应的实体属性字段)
		pageNum	是	Integer	当前页数
		pageSize	是	Integer	每页显示条数
	 * 
	 */
        findCommunityByToken(data){
            return lsxmRequest.get('elevatorInfo/findCommunityByToken', data)
        },
		/**
		 * @param {Object} data
		 * token	是	string	用户凭证
		communityId	是	Long	小区ID
		pageNum	是	Integer	当前页数
		pageSize	是	Integer	每页显示条数
		 */
		
		findElevatorInfoByCommunityId(data){
		    return lsxmRequest.get('elevatorInfo/findElevatorInfoByCommunityId', data)
		},
		
		
		/**
		 * @param {Object} data
		 * elevatorId	是	Long	电梯ID
		 */
		
		elevatorBaseInfo(data){
		    return lsxmRequest.get('elevatorInfo/elevatorBaseInfo', data)
		},
		
		/**
		 * 故障信息列表
		 * @param {Object} data
		 * 
		 */
		
		elevatorFaultList(data  = {}){
		    return lsxmRequest.get('elevatorFault/elevatorFaultList', data)
		},
		/**
		 * 电梯故障详情
		 * @param {Object} data
		 * 
		 */
		
		queryElevatorFaultDetails(data  = {}){
		    return lsxmRequest.get('elevatorFault/queryElevatorFaultDetails', data)
		},
		
		/**
		 * 困人记录
		 * @param {Object} data
		 * 
		 */
		
		elevatorTiringRecordList(data  = {}){
		    return lsxmRequest.get('elevatorTiring/elevatorTiringRecordList', data)
		},
		
		/**
		 * 困人列表
		 * @param {Object} data
		 * 
		 */
		
		elevatorTiringList(data  = {}){
		    return lsxmRequest.get('elevatorTiring/elevatorTiringList', data)
		},
		
		/**
		 * 保养记录
		 * @param {Object} data
		 * 
		 */
		
		selectElevatorUpkeepList(data  = {}){
		    return lsxmRequest.get('upkeep/selectElevatorUpkeepList', data)
		},
		
		/**
		 * 查询电梯的维修记录列表
		 * @param {Object} data
		 * 
		 */
		
		selectRepairListByElevator(data  = {}){
		    return lsxmRequest.get('elevatorFault/selectRepairListByElevator', data)
		},
		/**
		 * 困人详情
		 */
		elevatorTiringInfo(data  = {}){
		    return lsxmRequest.get('elevatorTiring/elevatorTiringInfo', data)
		},
		
		/**
		 * 我的救援
		 */
		myRescueList(data  = {}){
		    return lsxmRequest.get('elevatorTiring/myRescueList', data)
		},
		
		
		/**
		 * 救援-开始救援

		 */
		starRescue(data  = {}){
		    return lsxmRequest.post('elevatorTiring/starRescue', data)
		},
		/**
		 * 救援-签到
		
		 */
	
		rescueSign(data  = {}){
		    return lsxmRequest.post('elevatorTiring/rescueSign', data)
		},
		
		/**
		 * 救援-救援完成
		
		 */
		rescueComplete(data  = {}){
		    return lsxmRequest.post('elevatorTiring/rescueComplete', data)
		},
		/**
		 * 救援-救援完成
		
		 */
		confirmComplete(data  = {}){
		    return lsxmRequest.post('elevatorTiring/confirmComplete', data)
		},
		
		
		/**
		 * 查询维修详情页接口
		 */
		selectRepairDetail(data  = {}){
		    return lsxmRequest.get('elevatorFault/selectRepairDetail', data)
		},
		
		/**
		 * 开始维修接口

		 */
		marchElevatorRepair(data  = {}){
		    return lsxmRequest.post('elevatorFault/marchElevatorRepair', data)
		},
		/**
		 * 接受维修接口
		
		 */
		startElevatorRepair(data  = {}){
		    return lsxmRequest.post('elevatorFault/startElevatorRepair', data)
		},
		
		/**
		 * 完成维修接口
		
		 */
		submitElevatorRepair(data  = {}){
		    return lsxmRequest.post('elevatorFault/submitElevatorRepair', data)
		},
		
		// 年检
		selectAnnualInspectionList(data  = {}){
		    return lsxmRequest.get('annual/selectAnnualInspectionList', data)
		},
		
		// 年检 启动
		startAnnualInspection(data  = {}){
		    return lsxmRequest.post('annual/startAnnualInspection', data)
		},
		// 年检 确认
		confirmAnnualInspection(data  = {}){
		    return lsxmRequest.post('annual/confirmAnnualInspection', data)
		},
		// 年检 确认
		selectAnnualDetails(data  = {}){
		    return lsxmRequest.get('annual/selectAnnualDetails', data)
		},
		
		  
		
}
import lsxmRequest from '../config.js'
export default {
	/*
		选择电梯 获取搜索小区 /api/elevatorInfo/findCommunityByToken
	*/
	findCommunityByToken(data) {
		return lsxmRequest.get('elevatorInfo/findCommunityByToken', data)
	},

	/*
		获取电梯名称  /api/elevatorInfo/findElevatorInfoByCommunityId
	*/
	findElevatorInfoByCommunityId(data) {
		return lsxmRequest.post('elevatorInfo/findElevatorInfoByCommunityId', data)
	},

	/*
		上报困人
	*/
	reportElevatorTiring(data) {
		return lsxmRequest.post('elevatorTiring/reportElevatorTiring', data)
	},
	/*
		查询电梯名称
	*/
	elevatorBaseInfo(data) {
		return lsxmRequest.get('elevatorInfo/elevatorBaseInfo', data)
	},
	/*
		困人类型
	*/
	searchDictionList(data) {
		return lsxmRequest.get('sysDictionary/searchDictionList', data)
	},
	/*
		查询小区
	*/
	findCommunityByToken(data) {
		return lsxmRequest.get('elevatorInfo/findCommunityByToken', data)
	}, 
	/*
		查询电梯
	*/
	findElevatorInfoByCommunityId(data) {
		return lsxmRequest.get('elevatorInfo/findElevatorInfoByCommunityId', data)
	},
	/*
		电梯安装提交
	*/
	elevatorInstall(data) {
		return lsxmRequest.post('elevatorInfo/elevatorInstall', data)
	},
	/*
		安装记录
	*/
	elevatorInstallRecord(data) {
		return lsxmRequest.get('elevatorInfo/elevatorInstallRecord', data)
	},
	/*
		通知列表
	*/
	selectNoticeList(data) {
		return lsxmRequest.get('sysNotice/selectNoticeList', data)
	},
	
	/*
		通知详情
	*/
	selectNoticeInfo(data) {
		return lsxmRequest.get('sysNotice/selectNoticeInfo', data)
	},
	/*
		销售tab
	*/
	querySalesHeadNums(data) {
		return lsxmRequest.get('customer/querySalesHeadNums', data)
	},
	/*
		客户
	*/
	selectCustomerList(data) {
		return lsxmRequest.get('customer/selectCustomerList', data)
	},
	/*
		新增客户
	*/
	saveCustomerInfo(data) {
		return lsxmRequest.post('customer/saveCustomerInfo', data)
	},
	
	/*
		客户查询
	*/
	selectCustomerById(data) {
		return lsxmRequest.get('customer/selectCustomerById', data)
	},
	/*
		通讯录列表
	*/
	selectAddressBook(data) {
		return lsxmRequest.get('sysUser/selectAddressBook', data)
	},
	
	/*
		备忘录新增
	*/
	saveMemoNote(data) {
		return lsxmRequest.post('memoNote/saveMemoNote', data)
	},
	/*
		备忘录详情
	*/
	memoNoteDetail(data) {
		return lsxmRequest.post('memoNote/memoNoteDetail', data)
	},
	/*
		备忘录列表
	*/
	selectMemoNoteList(data) {
		return lsxmRequest.get('memoNote/selectMemoNoteList', data)
	},
	/*
		备忘录删除
	*/
	deleteMemoNote(data) {
		return lsxmRequest.post('memoNote/deleteMemoNote', data)
	},
	    
	/*
		反馈提交
	*/
	submitFeedbackMessage(data) {
		return lsxmRequest.post('feedback/submitFeedbackMessage', data)
	},
	


}

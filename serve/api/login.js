import lsxmRequest from '../config.js'
export default {
	/**
	 * 登录
	 * @param {loginName:账号 ，password：密码 ， rememberMe：0.不记住，1.记住 } data 
	 */
	getLoginInfo(data) {
		return lsxmRequest.post('sysUser/login', data)
	},

	/**
	 * 修改密码
	 */
	resetPwd(data = {}) {
		return lsxmRequest.post('sysUser/resetPwd', data)
	},

	// 忘记密码
	forgotPassword(data = {}) {
		return lsxmRequest.post('sysUser/forgotPassword', data)
	},


	/**
	 * 注册用户
	 * phoneNumber	是	string	手机号
		code	是	string	验证码
	 */
	register(data = {}) {
		return lsxmRequest.post('sysUser/register', data)
	},

	/**
	 * 获取验证码
	 * phoneNumber	是	string	手机号

	 */
	sendPhoneCode(data = {}) {
		return lsxmRequest.post('sysUser/sendPhoneCode', data)
	},



	/*
		选择企业公司调用公司列表接口
		/api/companyManage/findCompanyManageList
	*/
	findCompanyManageList(data = {}) {
		return lsxmRequest.get('companyManage/findCompanyManageList', data)
	},

	/*
		身份认证接口  提交审核
		/api/sysUser/identityAuthentication
	
	*/
	identityAuthentication(data = {}) {
		return lsxmRequest.post('sysUser/identityAuthentication', data)
	},

	
	/*
		版本更新
		/api/sysUser/identityAuthentication
	
	*/
	UpdateInfo(data = {}) {
		return lsxmRequest.post('version/info', data)
	},
	
	
	/*
		注册协议
		/api/sysUser/identityAuthentication
	
	*/
	registerAgreementDetail(data = {}) {
		return lsxmRequest.post('sysUser/registerAgreementDetail', data)
	},
	
	
}

import lsxmRequest from '../config.js'
export default{
	/*
		首页
	*/
        homeElevatorInfoList(data){
            return lsxmRequest.get('elevatorInfo/homeElevatorInfoList', data)
        },
		// 详情
		homeElevatorInfo(data){
		    return lsxmRequest.get('elevatorInfo/homeElevatorInfo', data)
		},
		
		// 困人数量
		elevatorTiringListNum(data){
		    return lsxmRequest.get('elevatorTiring/elevatorTiringListNum', data)
		},
		// 我的积分
		selectSysUserById(data){
		    return lsxmRequest.get('sysUser/selectSysUserById', data)
		},
		// 积分商品
		selectGoodsList(data){
		    return lsxmRequest.get('myIntegral/selectGoodsList', data)
		},
		// 积分规则
		selectIntegralRules(data){
		    return lsxmRequest.get('myIntegral/selectIntegralRules', data)
		},
		// 排行榜
		rankUserListByScore(data){
		    return lsxmRequest.get('myIntegral/rankUserListByScore', data)
		},
		// 积分明细
		selectIntegralDetailList(data){
		    return lsxmRequest.get('myIntegral/selectIntegralDetailList', data)
		},
		// 兑换记录
		selectOrderList(data){
		    return lsxmRequest.post('myIntegral/selectOrderList', data)
		},
		// 商品详情
		selectGoodsIntegralInfo(data){
		    return lsxmRequest.get('myIntegral/selectGoodsIntegralInfo', data)
		},
		// 生成预览订单
		immediatelyExchange(data){
		    return lsxmRequest.get('myIntegral/immediatelyExchange', data)
		},
		// 生成预览订单详情
		orderExchange(data){
		    return lsxmRequest.post('myIntegral/orderExchange', data)
		},
		
}
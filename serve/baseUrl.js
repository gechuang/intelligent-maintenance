const URL = {
    // 开发环境配置
    development: {
		baseUrl: 'http://127.0.0.1:8081/api/', // 后台接口请求地址
        websocketUrl: '', // websocket服务端地址
    },
    // 生产环境配置
    production: {
		baseUrl: 'http://192.168.3.74:8081/api/', // 后台接口请求地址
        websocketUrl: '', // websocket服务端地址
    }
};
//export default URL[process.env.NODE_ENV];

export default URL.production
//export default URL.development

import LsxmRequest from './LsxmRequest'
import $store from '../store/index.js';
import url from "./baseUrl.js"
let LogiFlg = true
const lsxmRequest = new LsxmRequest()


// 请求拦截器
lsxmRequest.interceptors.request((request) => {
	if (uni.getStorageSync('token')) {
		request.data['token'] = uni.getStorageSync('token');
	}
	return request
})

// 响应拦截器
lsxmRequest.interceptors.response((response) => {
	// console.log(response)
	if(LogiFlg){
		// 超时重新登录
		if (response.data.isOverTime) {
			loginAot("您已超时,请重新登录")
			LogiFlg =  false
		}else if(response.data.code === "401"){
			loginAot("请重新登录")
			LogiFlg =  false
		}else if(response.data.code === '1005'){
			loginAot(response.data.msg)
			LogiFlg =  false
		}else if(response.data.code === '1004'){
			loginAot("用户未登录")
			LogiFlg =  false
		}else if(response.data.code === '1009'){
			loginAot(response.data.msg)
			uni.reLaunch({
				url:"/pages/register/login/login"
			})
		}else if(response.data.code === "400"){
			uni.showToast({
				icon:"none",
				title: response.data.msg
			})
		}
		
		
	}
	if(response.data.code == 0 || response.data.code == 1){
		return response;
	}else{
		uni.showToast({
			icon:"none",
			title: response.data.msg
		})
	}
		
	
})

// 设置默认配置
lsxmRequest.setConfig((config) => {
	config.baseURL = url.baseUrl
	if (uni.getStorageSync('token')) {
		config.data['token'] = uni.getStorageSync('token');
	}
	return config;
})



function loginAot(text){
	uni.showModal({
		title: '提示',
		content: text,
		showCancel: false,
		icon: 'success', 
		success: function(e) {
			// console.log($store)
			if (e.confirm) {
				LogiFlg = true
				$store.commit('logout');
			}
		}
	});
}
export default lsxmRequest

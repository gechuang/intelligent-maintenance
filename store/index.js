import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		userInfo: uni.getStorageSync('userInfo') || {},
	},
	getters:{
		getUserInfo:state => state.userInfo, // 
	},
	mutations: { 
		setUserinfo(state, data) {
			state.userInfo = data;
			uni.setStorage({
			    key: 'userInfo',  
			    data: data,
				success() {
					uni.reLaunch({
						url:"/pages/index/index"
					})
				}
			})
		},
		
		logout(state) {
			state.userInfo = {}
			uni.removeStorageSync('token');
			uni.removeStorageSync('userInfo');
			uni.reLaunch({
				url:"/pages/register/login/login"
			}) 
	
		},
		
		
	},
	actions: {
		
	}
})

export default store

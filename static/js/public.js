// 手机号
export function checkPhone(val) {
	if (!(/^1[3456789]\d{9}$/.test(val))) {
		return false;
	} else {
		return true
	}
}



/**
 * @param {Object} text 提示文字
 */

export function tip(text) {
	uni.showToast({
		title: text,
		icon: "none"
	})
};

/**
 * 判断变量是否为空，
 * @param  {[type]}  param 变量
 * @return {Boolean}      为空返回true，否则返回false。
 */
export function isEmpty(param) {
	if (param) {
		var param_type = typeof(param);
		if (param_type == 'object') {
			//要判断的是【对象】或【数组】或【null】等
			if (typeof(param.length) == 'undefined') {
				if (JSON.stringify(param) == "{}") {
					return true; //空值，空对象
				}
			} else if (param.length == 0) {
				return true; //空值，空数组
			}
		} else if (param_type == 'string') {
			//如果要过滤空格等字符
			var new_param = param.trim();
			if (new_param.length == 0) {
				//空值，例如:带有空格的字符串" "。
				return true;
			}
		} else if (param_type == 'boolean') {
			if (!param) {
				return true;
			}
		} else if (param_type == 'number') {
			if (!param) {
				return true;
			}
		}
		return false; //非空值
	} else {

		//空值,例如：
		//(1)null
		//(2)可能使用了js的内置的名称，例如：var name=[],这个打印类型是字符串类型。
		//(3)空字符串''、""。
		//(4)数字0、00等，如果可以只输入0，则需要另外判断。
		return true;

	}
}

export function isUndefined(e) {
	return e == undefined || e == 'undefined' ? true : false
}


// 图片
export function isImage(str) {
	var reg = /\.(png|jpg|gif|jpeg|webp)$/;
	return reg.test(str);
}

// 是否为数组
export function isArray(o) {
	return Object.prototype.toString.call(o) == '[object Array]';
}


export function setDataValue(e, key) {
	var pages = getCurrentPages();
	var prevPage = pages[pages.length - 1];
	prevPage.$vm[key] = e.target.value

}


export function authentication(e) {
	if (e == 2) {
		return true
	} else if (e == 0) {
		uni.showToast({
			title: '请提交认证',
			icon: "none"
		})
		return false
	} else if (e == 1) {
		uni.showToast({
			title: '您的资料正在审核中,请耐心等待~',
			icon: "none"
		})
		return false
	} else if (e == 3) {
		uni.showToast({
			title: '审核未通过，请重新提交',
			icon: "none"
		})
		return false
	}

}


export function debounce(func, wait = 1000) {
	let timeout;
	return function(event) {
		clearTimeout(timeout)
		timeout = setTimeout(() => {
			func.call(this, event)
		}, wait)
	}
}

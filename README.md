### 平台简介

  &emsp;&emsp;智慧维保 APP 为电梯保险+服务创新管理新模式提供科技支撑。提供了电梯安全 大数据共享服务、电梯应急救援、电梯物联网、智慧电梯、电梯维保和合同签约等。为维保公司、物业公司、居民提供安全便捷的保障。

**智慧维保是一套开源的前端uniapp开发的APP，毫无保留给个人及企业免费使用。**

### 内置功能
&emsp;&emsp;1.签到功能：维修经理/工人/用户提供签到功能

&emsp;&emsp;2.保修功能：维修工人/用户随时上报电梯运行故障及电梯困人情况

&emsp;&emsp;3.电梯保养：定期定时对管理小区电梯进行保养

&emsp;&emsp;4.电梯维修：对上报故障/困人电梯维修人员及时处理

&emsp;&emsp;5.电梯年检：定期定时对管理小区电梯进行检查

&emsp;&emsp;6.电梯巡检：以小区为单位展示电梯运行状况

&emsp;&emsp;7.故障码管理：不同故障对应故障码,维修人员及时确定故障问题

&emsp;&emsp;8.电梯安装：提供电梯安装业务

&emsp;&emsp;9.配件申请：管理维修工人对配件申请/归还

&emsp;&emsp;10.大修管理：维修工人/用户随时上报电梯运行故障及电梯困人情况

&emsp;&emsp;12.销售管理：上报/维修获得的积分可在线兑换商品

### 演示图

![img.png](README_files/img.png)&emsp;&emsp; ![img_1.png](README_files/img_1.png)

![img_2.png](README_files/img_2.png)&emsp;&emsp; ![img_4.png](README_files/img_4.png)

![img_5.png](README_files/img_5.png)&emsp;&emsp; ![img_6.png](README_files/img_6.png)

![img_7.png](README_files/img_7.png)&emsp;&emsp; ![img_8.png](README_files/img_8.png)

### 后台系统大屏

![img_9.png](README_files/img_9.png)

### 交流群
&emsp;&emsp;QQ群:392286636

### 技术支持
&emsp;&emsp;由<a href="https://gechuang.net/" title="陕西格创网络科技有限公司">陕西格创网络科技有限公司</a>提供技术支持
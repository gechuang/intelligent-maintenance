import Vue from 'vue'
import App from './App'
// 1.引入封装的接口请求
// import {myRequest} from './api/api.js'

// // 2.挂载到页面上
// Vue.prototype.$myRuquest = myRequest
Vue.config.productionTip = false 

import {checkPhone , tip , isEmpty , isArray , isUndefined , setDataValue , authentication , debounce} from "static/js/public.js"
Vue.prototype.checkPhone = checkPhone
Vue.prototype.tip = tip
Vue.prototype.isEmpty = isEmpty
Vue.prototype.isArray = isArray
Vue.prototype.isUndefined = isUndefined
Vue.prototype.setDataValue = setDataValue


Vue.prototype.debounce = debounce

import store from "./store/index.js"
Vue.prototype.navRouter = function(url,isWod){
	if(isWod && authentication(store.getters.getUserInfo.reviewStatus)){
		uni.navigateTo({
			url:url
		})
	}else if(!isWod){
		uni.navigateTo({
			url:url
		})
	}
	
} 
Vue.prototype.backUrl = function(url){
	uni.navigateBack()
} 
Vue.prototype.reLaunch = function(url){
	uni.reLaunch({
		url:url
	})
} 

App.mpType = 'app'

const app = new Vue({
	
		store,
    ...App
})
app.$mount()
